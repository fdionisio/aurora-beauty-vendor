@extends('layouts.master')

@section('title') Data Tables @endsection

@section('headerCss')
    <!-- DataTables -->
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
    <!-- Lightbox -->
    <link href="{{ asset('plugins/filter/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
  <!-- start page title -->
<div class="row">

    @component('common-components.breadcrumb')
        @slot('title') Services @endslot                     
        @slot('li1') Aurora  @endslot
        @slot('li2') Services  @endslot
        @slot('li3') View All @endslot
    @endcomponent
        
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <a href="{{ url('services/add') }}" class="btn btn-primary px-4 btn-rounded float-right mt-0 mb-3">+ Add New Service</a>
                <div class="table-responsive">
                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Service Category</th>
                                <th>Price</th>
                                <th>Primary Image</th>
                                <th>Date Created</th>
                                <th>Date Updated</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($services as $service)
                            <tr>
                                <td><a href="{{ url('services') }}/{{Crypt::encryptString($service['id'])}}">{{$service['id']}}</a></td>
                                <td>{{$service['service_name']}}</td>
                                <td>{{$service['service_categories']['service_category_name']}}</td>
                                <td>{{$service['price']}}</td>
                                <td>
                                    @if(!empty($service['service_images']))
                                        @foreach($service['service_images'] as $image)
                                            @if($image['is_primary'] == 1)
                                            <div class="item-box">
                                                <a class="cbox-gallary1 mfp-image" href="{{ $image['image_url'] }}">
                                                    <img class="item-container mfp-fade w-25" src="{{ $image['image_url'] }}" alt="2" />
                                                </a>
                                            </div><!--end item-box-->
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>{{ date('M d, Y H:i:s a', strtotime($service['created_at']))}}</td>
                                <td>{{date('M d, Y H:i:s a', strtotime($service['updated_at']))}}</td>
                                <td>
                                    @if($service['is_active'] == 1)
                                        <span class="badge badge-success">Active</span>
                                    @else 
                                        <span class="badge badge-danger">Deactivated</span>  
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ url('services/update') }}/{{Crypt::encryptString($service['id'])}}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Update Record"><i class="mdi mdi-square-edit-outline mdi-24px"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
</div>
<!-- end row -->
@endsection

@section('footerScript')
    <!-- Required datatable js -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.datatable.init.js') }}"></script>

    <script src="{{ asset('plugins/filter/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.gallery.inity.js') }}"></script>
@endsection