@extends('layouts.master')

@section('title') Add a Service  @endsection

@section('headerCss')
  <!-- Dropify -->
  <link href="{{ asset('plugins/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
        @slot('title') Add a Service @endslot                     
        @slot('li1') Aurora  @endslot
        @slot('li2') Service  @endslot
        @slot('li3') Update @endslot
    @endcomponent 
</div>

<!-- end page title -->
<div class="row">
    <div class="col-lg-7">
        <div class="card">
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('error') }}
                    </div>
                @endif
                <form class="needs-validation" novalidate method="POST" action="{{ url('services/add') }}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label>Service Name</label>
                        <input type="text" class="form-control" required placeholder="Service Name" name="service_name" />
                    </div>

                    <div class="form-group">
                        <label>Service Description</label>
                        <textarea required class="form-control" rows="5" name="service_description" placeholder="Service Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Service Category</label>
                        <select required class="form-control" name="service_category_id">
                            <option>Please select a Service Category</option>
                            @foreach($categories as $serviceCategory)
                                <option value="{{ $serviceCategory['id'] }}">{{ $serviceCategory['service_category_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input id="demo2" type="text" value="0" name="price" class=" form-control">
                    </div>
                    <h5>Service Images</h5>
                    <div class="form-group mt-3">
                        <label>Primary Image</label>
                        <p class="text-muted mb-3">Primary image is the featured image for your service.</p>
                        <input type="file" id="input-file-now" class="dropify" accept="image/*" name="primary_image" data-max-file-size="3M" required/>                                              
                    </div>

                    <div class="form-group mt-3">
                        <label>Secondary Images</label>
                        <input type="file" id="input-file-now" class="dropify-secondary" accept="image/*" name="secondary_images[]" data-max-file-size="3M" multiple/>                                              
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="is_active" required>
                            <option value="">Select</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <a href="{{ url('services/view/all') }}" type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
    <!-- Parsley js -->
    <script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.validation.init.js') }}"></script> 
    <script src="{{ asset('plugins/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.form-upload.init.js') }}"></script>
    <script>$('.dropify-secondary').dropify();</script>
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.forms-advanced.js') }}"></script>

@endsection