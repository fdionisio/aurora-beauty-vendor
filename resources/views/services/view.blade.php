@extends('layouts.master')

@section('title') {{ $service->service_name }} @endsection

@section('headerCss')
    <!-- Lightbox css -->
    <link href="{{ asset('plugins/filter/magnific-popup.css')}}" rel="stylesheet" type="text/css" /> 
    <!-- DataTables -->
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
            @slot('title') {{ $service->service_name }} @endslot                     
            @slot('li1') Aurora  @endslot
            @slot('li2') {{ $service->service_name }} @endslot
            @slot('li3') View @endslot
    @endcomponent
</div>

<!-- Page Content-->
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body  met-pro-bg">
                        <div class="met-profile">
                            <div class="row">
                                <div class="col-lg-4 align-self-center mb-3 mb-lg-0">
                                    <div class="met-profile-main">
                                        <div class="met-profile-main-pic">
                                            @foreach($images as $image)
                                                @if($image['is_primary'] == TRUE)
                                                    <img src="{{ $image['image_url'] }}" alt="" class="rounded-circle w-100">
                                                @endif
                                            @endforeach
                                            <span class="fro-profile_main-pic-change">
                                                <i class="fas fa-camera"></i>
                                            </span>
                                        </div>
                                        <div class="met-profile_user-detail">
                                            <h5 class="met-user-name">{{ $service->service_name}}</h5>                                                        
                                            <p class="mb-0 met-user-name-post">&#8369; {{ $service->price }}</p>
                                        </div>
                                    </div>                                                
                                </div><!--end col-->
                                <div class="col-lg-4 ml-auto">
                                    <ul class="list-unstyled personal-detail">
                                        <li class=""><i class="mdi mdi-settings-outline mr-2"></i> <b> Service Status </b> : 
                                            @if($service->is_active == 1)
                                            <h6 class="text-success d-inline">Active<h6>
                                            @elseif($administrator->is_active == 0)
                                                <h6 class="text-danger d-inline">Deactivated</h6>
                                            @endif
                                        </li>
                                    </ul>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div><!--end f_profile-->                                                                                
                    </div><!--end card-body-->
                    <div class="card-body">
                        <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="general_detail_tab" data-toggle="pill" href="#general_detail">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="portfolio_detail_tab" data-toggle="pill" href="#service_images">Service Images</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="settings_detail_tab" data-toggle="pill" href="#reviews">Reviews</a>
                            </li>
                        </ul>        
                    </div><!--end card-body-->
                </div><!--end card-->
            </div><!--end col-->
        </div><!--end row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content detail-list" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="general_detail">
                        <div class="row">
                            <div class="col-12">                                            
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                @foreach($images as $image)
                                                    @if($image['is_primary'] == TRUE)
                                                        <img src="{{ $image['image_url'] }}" alt="" class="img-fluid">
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="col-md-6">
                                                <div class="met-basic-detail">
                                                    <h3>{{ $service->service_name}}</h3>
                                                    <p class="text-uppercase font-14">Graphic & Web Designer</p>
                                                    <p class="text-muted font-14">
                                                        {{ $service->service_description}}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>         
                                    </div><!--end card-body-->
                                </div><!--end card-->
                            </div><!--end col-->
                        </div><!--end row-->                                             
                    </div><!--end general detail-->

                    <!-- Service Images -->
                    <div class="tab-pane fade" id="service_images">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <ul class="col container-filter categories-filter mb-0" id="filter">
                                                <li><a class="categories active" data-filter="*">All</a></li>
                                                <li><a class="categories" data-filter=".primary">Primary</a></li>
                                                <li><a class="categories" data-filter=".secondary">Secondary</a></li>
                                            </ul>
                                        </div><!-- End portfolio  -->
                                    </div><!--end card-body-->
                                </div><!--end card-->
                                
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row container-grid nf-col-3 projects-wrapper">
                                            @foreach($images as $image)
                                                @if($image['is_primary'] == TRUE)
                                                    <div class="col-lg-4 col-md-6 p-0 nf-item primary">
                                                        <div class="item-box">
                                                            <a class="cbox-gallary1 mfp-image" href="{{ $image['image_url'] }}">
                                                                <img class="item-container" src="{{ $image['image_url'] }}" alt="7" />
                                                                <div class="item-mask">
                                                                    <div class="item-caption">
                                                                        <h5 class="text-white">{{ $service->service_name }}</h5>
                                                                        <p class="text-white">Primary</p>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div><!--end item-box-->
                                                    </div><!--end col-->
                                                @endif
                                            @endforeach
                                            @foreach($images as $image)
                                                @if($image['is_primary'] == FALSE)
                                                    <div class="col-lg-4 col-md-6 p-0 nf-item secondary">
                                                        <div class="item-box">
                                                            <a class="cbox-gallary1 mfp-image" href="{{ $image['image_url'] }}">
                                                                <img class="item-container" src="{{ $image['image_url'] }}" alt="7" />
                                                                <div class="item-mask">
                                                                    <div class="item-caption">
                                                                        <h5 class="text-white">{{ $service->service_name }}</h5>
                                                                        <p class="text-white">Secondary</p>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div><!--end item-box-->
                                                    </div><!--end col-->
                                                @endif
                                            @endforeach
                                        </div><!--end row-->
                                    </div><!--end card-body-->
                                </div><!--end card-->
                            </div><!--end col-->
                            <div class="col-lg-4">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="text-center">
                                            <h4><i class="fas fa-quote-left text-primary"></i></h4>
                                        </div>                                            
                                        <div id="carouselExampleFade2" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner">
                                                <div class="carousel-item">
                                                    <div class="text-center">
                                                        <p class="text-muted px-4">
                                                            It is a long established fact that a reader will be distracted by 
                                                            the readable content of a page when looking at its layout. 
                                                            The point of using Lorem Ipsum is that it has a more-or-less 
                                                            normal distribution of letters, as opposed to using.
                                                        </p>
                                                        <div class="">
                                                            <img src="../assets/images/users/user-10.jpg" alt="" class="rounded-circle thumb-lg mb-2">
                                                            <p class="mb-0 text-primary"><b>- Mary K. Myers</b></p>
                                                            <small class="text-muted">CEO Facebook</small>
                                                        </div>                                                            
                                                    </div>
                                                </div>
                                                <div class="carousel-item active">
                                                    <div class="text-center">
                                                        <p class="text-muted px-4">                                                                
                                                            Where does it come from?
                                                            Contrary to popular belief, Lorem Ipsum is not simply random text. 
                                                            It has roots in a piece of classical Latin literature from 45 BC, 
                                                            making it over 2000 years  popular belief,old.
                                                        </p>
                                                        <div class="">
                                                            <img src="../assets/images/users/user-4.jpg" alt="" class="rounded-circle  thumb-lg mb-2">
                                                            <p class="mb-0 text-primary"><b>- Michael C. Rios</b></p>
                                                            <small class="text-muted">CEO Facebook</small>
                                                        </div>                                                            
                                                    </div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="text-center">
                                                        <p class="text-muted px-4">
                                                            There are many variations of passages of Lorem Ipsum available, 
                                                            but the majority have suffered alteration in some form, by injected humour, 
                                                            or randomised words which don't look even slightly believable. 
                                                            If you are going to use a passage of Lorem Ipsum. 
                                                        </p>
                                                        <div class="">
                                                            <img src="../assets/images/users/user-5.jpg" alt="" class="rounded-circle  thumb-lg mb-2">
                                                            <p class="mb-0 text-primary"><b>- Lisa D. Pullen</b></p>
                                                            <small class="text-muted">CEO Facebook</small>
                                                        </div>                                                            
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--end row-->
                    </div><!--end portfolio detail-->
                    
                    <div class="tab-pane fade" id="reviews">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Customer</th>
                                                    <th>Service</th>
                                                    <th>Employee</th>
                                                    <th>Date</th>
                                                    <th>Rate</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tiger Nixon</td>
                                                    <td>System Architect</td>
                                                    <td>Edinburgh</td>
                                                    <td>61</td>
                                                    <td>2011/04/25</td>
                                                    <td>$320,800</td>
                                                </tr>
                                                <tr>
                                                    <td>Garrett Winters</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>63</td>
                                                    <td>2011/07/25</td>
                                                    <td>$170,750</td>
                                                </tr>
                                                <tr>
                                                    <td>Ashton Cox</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>San Francisco</td>
                                                    <td>66</td>
                                                    <td>2009/01/12</td>
                                                    <td>$86,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cedric Kelly</td>
                                                    <td>Senior Javascript Developer</td>
                                                    <td>Edinburgh</td>
                                                    <td>22</td>
                                                    <td>2012/03/29</td>
                                                    <td>$433,060</td>
                                                </tr>
                                                <tr>
                                                    <td>Airi Satou</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>33</td>
                                                    <td>2008/11/28</td>
                                                    <td>$162,700</td>
                                                </tr>
                                                <tr>
                                                    <td>Brielle Williamson</td>
                                                    <td>Integration Specialist</td>
                                                    <td>New York</td>
                                                    <td>61</td>
                                                    <td>2012/12/02</td>
                                                    <td>$372,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Herrod Chandler</td>
                                                    <td>Sales Assistant</td>
                                                    <td>San Francisco</td>
                                                    <td>59</td>
                                                    <td>2012/08/06</td>
                                                    <td>$137,500</td>
                                                </tr>
                                                <tr>
                                                    <td>Rhona Davidson</td>
                                                    <td>Integration Specialist</td>
                                                    <td>Tokyo</td>
                                                    <td>55</td>
                                                    <td>2010/10/14</td>
                                                    <td>$327,900</td>
                                                </tr>
                                                <tr>
                                                    <td>Colleen Hurst</td>
                                                    <td>Javascript Developer</td>
                                                    <td>San Francisco</td>
                                                    <td>39</td>
                                                    <td>2009/09/15</td>
                                                    <td>$205,500</td>
                                                </tr>
                                                <tr>
                                                    <td>Sonya Frost</td>
                                                    <td>Software Engineer</td>
                                                    <td>Edinburgh</td>
                                                    <td>23</td>
                                                    <td>2008/12/13</td>
                                                    <td>$103,600</td>
                                                </tr>
                                                <tr>
                                                    <td>Jena Gaines</td>
                                                    <td>Office Manager</td>
                                                    <td>London</td>
                                                    <td>30</td>
                                                    <td>2008/12/19</td>
                                                    <td>$90,560</td>
                                                </tr>
                                                <tr>
                                                    <td>Quinn Flynn</td>
                                                    <td>Support Lead</td>
                                                    <td>Edinburgh</td>
                                                    <td>22</td>
                                                    <td>2013/03/03</td>
                                                    <td>$342,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Charde Marshall</td>
                                                    <td>Regional Director</td>
                                                    <td>San Francisco</td>
                                                    <td>36</td>
                                                    <td>2008/10/16</td>
                                                    <td>$470,600</td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                    </div>                                            
                                </div>
                            </div> <!--end col-->                                          
                        </div><!--end row-->
                    </div><!--end settings detail-->
                </div><!--end tab-content--> 
                
            </div><!--end col-->
        </div><!--end row-->

    </div><!-- container -->

</div>
<!-- end page content -->
@endsection

@section('footerScript')
    <!-- Magnific Popup-->
    <script src="{{ asset('pages/jquery.profile.init.js') }}"></script>
    <script src="{{ asset('plugins/filter/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/filter/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('plugins/filter/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.gallery.inity.js') }}"></script>
    <!-- Required datatable js -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Buttons examples -->
    <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/buttons.colVis.min.js') }}"></script>
    <!-- Responsive examples -->
    <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.datatable.init.js') }}"></script>
@endsection