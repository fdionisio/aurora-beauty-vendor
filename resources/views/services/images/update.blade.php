@extends('layouts.master')

@section('title') Update Service Image  @endsection

@section('headerCss')
  <!-- Dropify -->
  <link href="{{ asset('plugins/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
            @slot('title') Update Service Image @endslot                     
            @slot('li1') Aurora  @endslot
            @slot('li2') Service Images  @endslot
            @slot('li3') Update @endslot
    @endcomponent

    @component('common-components.chart')
            @slot('chart1_id') header-chart-1  @endslot                     
            @slot('chart1_title') Balance $ 2,317  @endslot
            @slot('chart2_id') header-chart-2  @endslot                     
            @slot('chart3_title') Item Sold 1230  @endslot
    @endcomponent    
</div>
<!-- end page title -->
<div class="row">
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <form class="needs-validation" novalidate method="POST" action="{{ url('services/images/update') }}/{{ Crypt::encryptString($image->id) }}" enctype="multipart/form-data">
                @csrf
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('success') }}
                        </div>
                    @elseif(session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('error') }}
                        </div>
                    @endif
                    <div class="form-group mt-3">
                        <input type="file" id="input-file-now" class="dropify" accept="image/*" name="service_image" data-default-file="{{ $image->image_url }}" data-max-file-size="3M"/>                     
                        <input type="hidden" name="old_image" value="{{$image->image_file}}">
                    </div>
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" value="TRUE" name="is_primary" id="exampleCheck1"
                            @if($image->is_primary == TRUE)
                                {{'checked'}}
                            @endif
                        >
                        <label class="form-check-label" for="exampleCheck1">Set as primary image.</label>
                        <input type="hidden" name="service_id" value="{{$image->service_id}}">
                    </div> 
                    <div class="form-group mt-3 text-right">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                               Update
                            </button>
                            <a href="{{ url('services/view/all') }}" type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
<script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.validation.init.js') }}"></script> 
    <script src="{{ asset('plugins/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.form-upload.init.js') }}"></script>
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('/js/pages/form-validation.init.js')}}"></script>
@endsection