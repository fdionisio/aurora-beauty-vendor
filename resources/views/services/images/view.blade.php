@extends('layouts.master')

@section('title') Manage Service Images  @endsection

@section('headerCss')
    <!-- Lightbox css -->
    <link href="{{ asset('plugins/filter/magnific-popup.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
            @slot('title') Update Service Image @endslot                     
            @slot('li1') Aurora  @endslot
            @slot('li2') Service Images  @endslot
            @slot('li3') Manage @endslot
    @endcomponent  
</div>
<!-- end page title -->
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Manage Images</h4>
                <a href="{{ url('services/images/insert') }}/{{ Crypt::encryptString($serviceId) }}" type="button" class="btn btn-primary waves-effect waves-light float-right">Add Image</a>
            </div>
            <div class="card-body">
                <div class="pb-4">
                    <h5>Primary Image</h5>
                    @foreach($images as $image)
                        @if($image['is_primary'] == TRUE)
                        <div class="col-md-3 card p-3">
                            <div class="item-box text-center">
                                <a class="cbox-gallary1 mfp-image" href="{{ $image['image_url'] }}">
                                    <img class="item-container mfp-fade w-50" src="{{ $image['image_url'] }}" alt="2" />
                                </a>
                            </div><!--end item-box-->
                            <div class="text-right">
                                <a href="{{ url('services/images/update') }}/{{ Crypt::encryptString($image['id']) }}" class="btn btn-primary waves-effect waves-light mt-3">
                                   Update
                                </a>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div>
                <div>
                    <h5>Secondary Images</h5>
                    <div class="row">
                        @foreach($images as $image)
                            @if($image['is_primary'] == FALSE)
                            <div class="col-md-3 card p-3">
                                <div class="item-box text-center">
                                    <a class="cbox-gallary1 mfp-image" href="{{ $image['image_url'] }}">
                                        <img class="item-container mfp-fade w-50" src="{{ $image['image_url'] }}" alt="2" />
                                    </a>
                                </div><!--end item-box-->
                                <div class="text-right">
                                    <a href="{{ url('services/images/update') }}/{{ Crypt::encryptString($image['id']) }}" class="btn btn-primary waves-effect waves-light mt-3">
                                        Update
                                    </a>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
    <script src="{{ asset('plugins/filter/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.gallery.inity.js') }}"></script>
@endsection