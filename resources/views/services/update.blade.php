@extends('layouts.master')

@section('title') Update Service  @endsection

@section('headerCss')
    <!-- Touchspin -->
    <link href="{{ asset('plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
            @slot('title') Update Service @endslot                     
            @slot('li1') Aurora  @endslot
            @slot('li2') Service  @endslot
            @slot('li3') Update {{ $service['service_name'] }} @endslot
    @endcomponent
</div>
<!-- end page title -->
<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('error') }}
                    </div>
                @endif
                <form class="needs-validation" novalidate method="POST" action="{{ url('services/update') }}/{{Crypt::encryptString($service['id'])}}">
                @csrf
                    <div class="form-group">
                        <label>Service Name</label>
                        <input type="text" class="form-control" placeholder="Service Name" value="{{ $service['service_name'] }}" name="service_name" required />
                    </div>

                    <div class="form-group">
                        <label>Service Description</label>
                        <textarea class="form-control" rows="5" name="service_description" placeholder="Service Description" required>{{ $service['service_description'] }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Service Category</label>
                        <select required class="form-control" name="service_category_id">
                            <option>Please select a Service Category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category['id'] }}"
                                    @if($category['id'] == $service['service_category_id'])
                                        {{'selected'}}
                                    @endif
                                >{{ $category['service_category_name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Price</label>
                        <input id="demo2" type="text" value="{{ $service['price'] }}" name="price" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <h5>Service Images</h5>
                        @if( !empty($service['service_images']) )
                            <a href="{{ url('services/images/manage') }}/{{ Crypt::encryptString($service['id']) }}" type="button" class="btn btn-primary waves-effect waves-light">Manage Images</a>
                        @elseif($service['service_images'] == NULL)
                            <a href="{{ url('services/images/add') }}/{{ Crypt::encryptString($service['id']) }}" type="button" class="btn btn-primary waves-effect waves-light">Add Images</a>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control" name="is_active" required>
                            <option>Select</option>
                            <option value="1"
                                @if($service['is_active'] == 1)
                                    {{'selected'}}
                                @endif
                            >Active</option>
                            <option value="0"
                                @if($service['is_active'] == 0)
                                    {{'selected'}}
                                @endif
                            >Deactivated</option>
                        </select>
                    </div>
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <a href="{{ url('services/view/all') }}" type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footerScript')
    <!-- Parsley js -->
    <script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.validation.init.js') }}"></script> 
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.forms-advanced.js') }}"></script>
@endsection