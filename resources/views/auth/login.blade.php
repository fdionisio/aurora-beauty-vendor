@extends('layouts.auth-master')
@section('title', 'Login')
@section('content')
<div class="row vh-100">
    <div class="col-12 align-self-center">
        <div class="auth-page">
            <div class="card auth-card shadow-lg">
                <div class="card-body pt-0">
                    <h3 class="text-center mt-4">
                        <a href="/" class="logo logo-admin"><img src="{{ asset('/images/Aurora-logo-black.png')}}" height="70" alt="logo"></a>
                    </h3>
                    <div class="p-3">
                        <h4 class="text-muted font-size-18 mb-1 text-center">Welcome Back !</h4>
                        <p class="text-muted text-center">Sign in to continue to Aurora.</p>
                        <form method="POST" class="form-horizontal auth-form my-4" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="username">E-Mail Address</label>
                                <div class="input-group mb-3">
                                    <span class="auth-form-icon">
                                        <i class="dripicons-mail"></i> 
                                    </span>      
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="form-group">
                                <label for="userpassword">Password</label>
                                <div class="input-group mb-3"> 
                                    <span class="auth-form-icon">
                                        <i class="dripicons-lock"></i> 
                                    </span>   
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mt-4">
                                <div class="col-sm-6">
                                    <div class="custom-control custom-switch switch-success">
                                        <input type="checkbox" class="custom-control-input" name="remember" id="customControlInline" {{ old('remember') ? 'checked' : '' }}>
                                        <label class="custom-control-label text-muted" for="customControlInline">{{ __('Remember Me') }}</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>
                            <div class="form-group mb-0 row">
                                <div class="col-12 mt-2">
                                    <button class="btn btn-primary btn-round btn-block waves-effect waves-light" type="submit">Log In <i class="fas fa-sign-in-alt ml-1"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="account-social text-center mt-4">
                <h6 class="my-4">Or Login With</h6>
                <ul class="list-inline mb-4">
                    <li class="list-inline-item">
                        <a href="" class="">
                            <i class="fab fa-facebook-f facebook"></i>
                        </a>                                    
                    </li>
                    <li class="list-inline-item">
                        <a href="" class="">
                            <i class="fab fa-twitter twitter"></i>
                        </a>                                    
                    </li>
                    <li class="list-inline-item">
                        <a href="" class="">
                            <i class="fab fa-google google"></i>
                        </a>                                    
                    </li>
                </ul>
            </div><!--end account-social-->
            <div class="mt-5 text-center">
                <p>Don't have an account ? <a href="/register" class="text-primary"> Signup Now </a></p>
                <p>© {{  date('Y', strtotime('-0 year')) }} Aurora.
            </div>
        </div>
    </div>
</div>
@endsection
