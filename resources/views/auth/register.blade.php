@extends('layouts.auth-master')
@section('title', 'Signup')
@section('content')
<div class="row vh-100 mt-3 mb-3">
    <div class="col-12 align-self-center">
        <div class="auth-page">
            <div class="card auth-card shadow-lg">
                    <div class="card-body pt-0">
                        <h3 class="text-center mt-4">
                            <a href="/" class="logo logo-admin"><img src="{{ asset('/images/Aurora-logo-black.png')}}" height="70" alt="logo"></a>
                        </h3>
                        <div class="text-center auth-logo-text">
                            <p class="text-muted mb-0">Register your account now.</p>  
                        </div> <!--end auth-logo-text-->  
                        <div class="p-3">
                            <form method="POST" action="{{ route('register') }}" class="form-horizontal auth-form my-4">
                                @csrf
                                <div class="form-group">
                                    <label for="name">{{ __('Store Name') }}</label>
                                    <div class="input-group mb-3">
                                        <span class="auth-form-icon">
                                            <i class="dripicons-user"></i> 
                                        </span>
                                        <input id="name" type="text" class="form-control @error('store_name') is-invalid @enderror" name="store_name" value="{{ old('store_name') }}" required autocomplete="store_name" autofocus>

                                        @error('store_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="name">{{ __('Contact Person') }}</label>
                                    <div class="input-group mb-3">
                                        <span class="auth-form-icon">
                                            <i class="dripicons-user"></i>
                                        </span>
                                        <input id="name" type="text" class="form-control @error('contact_person') is-invalid @enderror" name="contact_person" value="{{ old('contact_person') }}" required autocomplete="name" autofocus>
                                        @error('contact_')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                                    <div class="input-group mb-3">
                                        <span class="auth-form-icon">
                                            <i class="dripicons-mail"></i> 
                                        </span>
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">{{ __('Mobile Number') }}</label>
                                    <div class="input-group mb-3"> 
                                        <span class="auth-form-icon">
                                            <i class="dripicons-phone"></i> 
                                        </span>         
                                        <input id="email" type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" value="{{ old('mobile_number') }}" required autocomplete="mobile_number">
                                        @error('mobile_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password">{{ __('Password') }}</label>
                                    <div class="input-group mb-3"> 
                                        <span class="auth-form-icon">
                                            <i class="dripicons-lock"></i> 
                                        </span>        
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                    <div class="input-group mb-3"> 
                                        <span class="auth-form-icon">
                                            <i class="dripicons-lock-open"></i> 
                                        </span>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>

                                <div class="form-group row mt-4">
                                    <div class="col-sm-12">
                                        <div class="custom-control custom-switch switch-success">
                                            <input type="checkbox" class="custom-control-input" id="customSwitchSuccess" required>
                                            <label class="custom-control-label text-muted" for="customSwitchSuccess">By registering you agree to the Aurora <a href="#" class="text-primary">Terms of Use</a></label>
                                        </div>
                                    </div><!--end col-->                                             
                                </div><!--end form-group--> 
                                <div class="form-group row mb-0">
                                    <div class="col-12 mt-2">
                                        <button type="submit" class="btn btn-primary btn-round btn-block waves-effect waves-light">
                                            {{ __('Sign up') }}
                                            <i class="fas fa-sign-in-alt ml-1"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <div class="text-center">
                    <p>Already have an account ? <a href="/login" class="text-primary"> Login </a> </p>
                    <p>© {{  date('Y', strtotime('-0 year')) }} Aurora.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
