@extends('layouts.master')

@section('title') Add Employee  @endsection

@section('headerCss')
  <!-- Dropify -->
  <link href="{{ asset('plugins/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- Multiselect -->
  <link href="{{ asset('plugins/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />
  <!-- Time Picker -->
  <link href="{{ asset('plugins/timepicker/bootstrap-material-datetimepicker.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- start page title --> 
<div class="row"> 
    @component('common-components.breadcrumb')
        @slot('title') Add Employee @endslot                     
        @slot('li1') Aurora  @endslot
        @slot('li2') Employee  @endslot
        @slot('li3') Add @endslot
    @endcomponent 
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('error') }}
                    </div>
                @endif
                <form class="needs-validation" novalidate method="POST" action="{{ url('employees/add') }}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group">
                        <label>First Name</label>
                        <input type="text" class="form-control" required placeholder="First Name" name="first_name" />
                        @error('first_name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control" required placeholder="Last Name" name="last_name" />
                        @error('last_name')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" required placeholder="Email Address" name="email" />
                        @error('email')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Contact Number</label>
                        <input type="text" class="form-control" required placeholder="Contact Number" name="contact_number" />
                        @error('contact_number')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Start Shift</label>
                        <input class="form-control" name="start_work_hour" id="timepicker" placeholder="Start Shift" required>
                        @error('start_work_hour')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>End Shift</label>
                        <input class="form-control" name="end_work_hour" id="timepicker2" placeholder="End Shift" required>
                        @error('end_work_hour')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Leaves</label>
                        <select class="select2 mb-3 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose Day/s" name="employee_leave[]">
                            <optgroup>
                                <option value="Monday">Monday</option>
                                <option value="Tuesday">Tuesday</option>
                                <option value="Wednesday">Wednesday</option>
                                <option value="Thursday">Thursday</option>
                                <option value="Friday">Friday</option>
                                <option value="Saturday">Saturday</option>
                                <option value="Sunday">Sunday</option>
                            </optgroup>
                        </select> 
                    </div>
                    <div class="form-group">
                        <label>Services Assigned</label>
                        <select class="select2 mb-3 select2-multiple" style="width: 100%" multiple="multiple" data-placeholder="Choose Services" name="employee_service[]" required>
                            <optgroup>
                                @foreach($services as $service)
                                    <option value="{{$service['id']}}">{{ $service['service_name'] }}</option>
                                @endforeach
                            </optgroup>
                        </select> 
                    </div>
                    <div class="form-group mt-3">
                        <label>Profile Photo</label>
                        <input type="file" id="input-file-now" class="dropify" accept="image/*" name="profile_photo" data-max-file-size="3M" required/>                                              
                    </div>
                    <div class="form-group">
                        <label>Employee Status</label>
                        <select class="form-control" name="is_active" required>
                            <option value="">Select</option>
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                    <div class="form-group mb-0">
                        <div>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                Submit
                            </button>
                            <a href="{{ url('employees/view/all') }}" type="reset" class="btn btn-secondary waves-effect">
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
 
@endsection


@section('footerScript')
    <!-- Parsley js -->
    <script src="{{ asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.validation.init.js') }}"></script> 
    <script src="{{ asset('plugins/dropify/js/dropify.min.js') }}"></script>
    <script src="{{ asset('plugins/select2/select2.min.js') }}"></script>
    <script src="{{ asset('plugins/timepicker/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('pages/jquery.form-upload.init.js') }}"></script>
    <script src="{{ asset('js/jquery.core.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js') }}"></script>
    <script src="{{ asset('pages/jquery.forms-advanced.js') }}"></script>

@endsection