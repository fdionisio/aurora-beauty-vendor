@extends('layouts.master')

@section('title') {{ $employee[0]['first_name'] }} {{ $employee[0]['last_name'] }} Record @endsection

@section('headerCss')
    <!-- Lightbox css -->
    <link href="{{ asset('plugins/filter/magnific-popup.css')}}" rel="stylesheet" type="text/css" /> 
    <!-- DataTables -->
    <link href="{{ asset('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ asset('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" /> 
    <!--calendar css-->
    <link href="{{ asset('plugins/fullcalendar/packages/core/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/fullcalendar/packages/daygrid/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/fullcalendar/packages/bootstrap/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/fullcalendar/packages/timegrid/main.css') }}" rel="stylesheet" />
    <link href="{{ asset('plugins/fullcalendar/packages/list/main.css') }}" rel="stylesheet" />

@endsection

@section('content')
<!-- start page title -->
<div class="row">
    @component('common-components.breadcrumb')
            @slot('title') {{ $employee[0]['first_name'] }} {{ $employee[0]['last_name'] }}@endslot                     
            @slot('li1') Aurora  @endslot
            @slot('li2') {{ $employee[0]['first_name'] }} {{ $employee[0]['last_name'] }} Record @endslot
            @slot('li3') View @endslot
    @endcomponent
</div>

<!-- Page Content-->
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body  met-pro-bg">
                        <div class="met-profile">
                            <div class="row">
                                <div class="col-lg-4 align-self-center mb-3 mb-lg-0">
                                    <div class="met-profile-main">
                                        <div class="met-profile-main-pic">
                                            <img src="{{ $employee[0]['profile_photo'] }}" alt="" class="rounded-circle w-100">
                                            <span class="fro-profile_main-pic-change">
                                                <i class="fas fa-camera"></i>
                                            </span>
                                        </div>
                                        <div class="met-profile_user-detail">
                                            <h5 class="met-user-name">{{ $employee[0]['first_name'] }} {{ $employee[0]['last_name'] }}</h5>                                                        
                                        </div>
                                    </div>                                                
                                </div><!--end col-->
                                <div class="col-lg-4 ml-auto">
                                    <ul class="list-unstyled personal-detail">
                                        <li class=""><i class="mdi mdi-settings-outline mr-2"></i> <b> Employee Status </b> : 
                                            @if($employee[0]['is_active'] == 1)
                                            <h6 class="text-success d-inline">Active<h6>
                                            @elseif($employee[0]['is_active'] == 0)
                                                <h6 class="text-danger d-inline">Deactivated</h6>
                                            @endif
                                        </li>
                                    </ul>
                                </div><!--end col-->
                            </div><!--end row-->
                        </div><!--end f_profile-->                                                                                
                    </div><!--end card-body-->
                    <div class="card-body">
                        <ul class="nav nav-pills mb-0" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="general_detail_tab" data-toggle="pill" href="#general_detail">General</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="portfolio_detail_tab" data-toggle="pill" href="#service_images">Service History</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="settings_detail_tab" data-toggle="pill" href="#reviews">Schedule</a>
                            </li>
                            <li class="nav-item">
                                <a class="btn btn-primary waves-effect waves-light" href="{{ url('employees/update') }}/{{Crypt::encryptString($employee[0]['id'])}}">Update</a>
                            </li>
                        </ul>        
                    </div><!--end card-body-->
                </div><!--end card-->
            </div><!--end col-->
        </div><!--end row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content detail-list" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="general_detail">
                        <div class="row">
                            <div class="col-12">                                            
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <img src="{{ $employee[0]['profile_photo'] }}" alt="" class="img-fluid">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="met-basic-detail">
                                                    <h3>{{ $employee[0]['first_name'] }} {{ $employee[0]['last_name'] }}</h3>
                                                    @foreach($employee[0]['service'] as $service)
                                                        <span class="badge badge-secondary">{{$service['services']['service_name']}}</span>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>         
                                    </div><!--end card-body-->
                                </div><!--end card-->
                            </div><!--end col-->
                        </div><!--end row-->                                             
                    </div><!--end general detail-->

                    <!-- Service Images -->
                    <div class="tab-pane fade" id="service_images">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Customer</th>
                                                    <th>Service</th>
                                                    <th>Employee</th>
                                                    <th>Date</th>
                                                    <th>Rate</th>
                                                    <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Tiger Nixon</td>
                                                    <td>System Architect</td>
                                                    <td>Edinburgh</td>
                                                    <td>61</td>
                                                    <td>2011/04/25</td>
                                                    <td>$320,800</td>
                                                </tr>
                                                <tr>
                                                    <td>Garrett Winters</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>63</td>
                                                    <td>2011/07/25</td>
                                                    <td>$170,750</td>
                                                </tr>
                                                <tr>
                                                    <td>Ashton Cox</td>
                                                    <td>Junior Technical Author</td>
                                                    <td>San Francisco</td>
                                                    <td>66</td>
                                                    <td>2009/01/12</td>
                                                    <td>$86,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Cedric Kelly</td>
                                                    <td>Senior Javascript Developer</td>
                                                    <td>Edinburgh</td>
                                                    <td>22</td>
                                                    <td>2012/03/29</td>
                                                    <td>$433,060</td>
                                                </tr>
                                                <tr>
                                                    <td>Airi Satou</td>
                                                    <td>Accountant</td>
                                                    <td>Tokyo</td>
                                                    <td>33</td>
                                                    <td>2008/11/28</td>
                                                    <td>$162,700</td>
                                                </tr>
                                                <tr>
                                                    <td>Brielle Williamson</td>
                                                    <td>Integration Specialist</td>
                                                    <td>New York</td>
                                                    <td>61</td>
                                                    <td>2012/12/02</td>
                                                    <td>$372,000</td>
                                                </tr>
                                                <tr>
                                                    <td>Herrod Chandler</td>
                                                    <td>Sales Assistant</td>
                                                    <td>San Francisco</td>
                                                    <td>59</td>
                                                    <td>2012/08/06</td>
                                                    <td>$137,500</td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--end portfolio detail-->
                    
                    <div class="tab-pane fade" id="reviews">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div id='calendar'></div>
                                        <div style='clear:both'></div>
                                    </div>                                            
                                </div>
                            </div> <!--end col-->                                          
                        </div><!--end row-->
                    </div><!--end settings detail-->
                </div><!--end tab-content--> 
                
            </div><!--end col-->
        </div><!--end row-->

    </div><!-- container -->

</div>
<!-- end page content -->
@endsection
@section('footerScript')
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/packages/core/main.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/packages/daygrid/main.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/packages/timegrid/main.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/packages/interaction/main.js') }}"></script>
    <script src="{{ asset('plugins/fullcalendar/packages/list/main.js') }}"></script>
    <script src="{{ asset('pages/jquery.calendar.js') }}"></script>
@endsection
