<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                © {{  date('Y') }} Aurora</span>
            </div>
        </div>
    </div>
</footer>