
    <!-- Left Sidenav -->
    <div class="left-sidenav">
        <div class="main-icon-menu">
            <nav class="nav">
                <a href="#MetricaAnalytic" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Analytics">
                    <svg class="nav-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <path d="M184,448h48c4.4,0,8-3.6,8-8V72c0-4.4-3.6-8-8-8h-48c-4.4,0-8,3.6-8,8v368C176,444.4,179.6,448,184,448z"/>
                            <path class="svg-primary" d="M88,448H136c4.4,0,8-3.6,8-8V296c0-4.4-3.6-8-8-8H88c-4.4,0-8,3.6-8,8V440C80,444.4,83.6,448,88,448z"/>
                            <path class="svg-primary" d="M280.1,448h47.8c4.5,0,8.1-3.6,8.1-8.1V232.1c0-4.5-3.6-8.1-8.1-8.1h-47.8c-4.5,0-8.1,3.6-8.1,8.1v207.8
                                C272,444.4,275.6,448,280.1,448z"/>
                            <path d="M368,136.1v303.8c0,4.5,3.6,8.1,8.1,8.1h47.8c4.5,0,8.1-3.6,8.1-8.1V136.1c0-4.5-3.6-8.1-8.1-8.1h-47.8
                                C371.6,128,368,131.6,368,136.1z"/>
                        </g>
                    </svg>
                </a><!--end MetricaAnalytic-->

                <a href="#MetricaCrypto" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Services">
                    <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M261 149.3V64H48v384h416V149.3H261zm-127.8 256H90.6v-42.7h42.6v42.7zm0-85.3H90.6v-42.7h42.6V320zm0-85.3H90.6V192h42.6v42.7zm0-85.4H90.6v-42.7h42.6v42.7zm85.2 256h-42.6v-42.7h42.6v42.7zm0-85.3h-42.6v-42.7h42.6V320zm0-85.3h-42.6V192h42.6v42.7zm0-85.4h-42.6v-42.7h42.6v42.7zm203 256H261v-42.7h42.6V320H261v-42.7h42.6v-42.7H261V192h160.4v213.3zm-37.6-170.6h-42.6v42.7h42.6v-42.7zm0 85.3h-42.6v42.7h42.6V320z"></path>
                    </svg>
                </a><!--end MetricaCrypto-->

                <a href="#employees" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Employees">
                    <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M337.454 232c33.599 0 61.092-27.002 61.092-60 0-32.997-27.493-60-61.092-60s-61.09 27.003-61.09 60c0 32.998 27.491 60 61.09 60zm-162.908 0c33.599 0 61.09-27.002 61.09-60 0-32.997-27.491-60-61.09-60s-61.092 27.003-61.092 60c0 32.998 27.493 60 61.092 60zm0 44C126.688 276 32 298.998 32 346v54h288v-54c0-47.002-97.599-70-145.454-70zm162.908 11.003c-6.105 0-10.325 0-17.454.997 23.426 17.002 32 28 32 58v54h128v-54c0-47.002-94.688-58.997-142.546-58.997z"></path></svg>
                </a><!--end MetricaProject-->

                <a href="#MetricaEcommerce" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Ecommerce">
                    <svg class="nav-svg" version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <ellipse class="svg-primary" transform="matrix(0.9998 -1.842767e-02 1.842767e-02 0.9998 -7.7858 3.0205)" cx="160" cy="424" rx="24" ry="24"/>
                            <ellipse class="svg-primary" transform="matrix(2.381651e-02 -0.9997 0.9997 2.381651e-02 -48.5107 798.282)" cx="384.5" cy="424" rx="24" ry="24"/>
                            <path d="M463.8,132.2c-0.7-2.4-2.8-4-5.2-4.2L132.9,96.5c-2.8-0.3-6.2-2.1-7.5-4.7c-3.8-7.1-6.2-11.1-12.2-18.6
                                c-7.7-9.4-22.2-9.1-48.8-9.3c-9-0.1-16.3,5.2-16.3,14.1c0,8.7,6.9,14.1,15.6,14.1c8.7,0,21.3,0.5,26,1.9c4.7,1.4,8.5,9.1,9.9,15.8
                                c0,0.1,0,0.2,0.1,0.3c0.2,1.2,2,10.2,2,10.3l40,211.6c2.4,14.5,7.3,26.5,14.5,35.7c8.4,10.8,19.5,16.2,32.9,16.2h236.6
                                c7.6,0,14.1-5.8,14.4-13.4c0.4-8-6-14.6-14-14.6H189h-0.1c-2,0-4.9,0-8.3-2.8c-3.5-3-8.3-9.9-11.5-26l-4.3-23.7
                                c0-0.3,0.1-0.5,0.4-0.6l277.7-47c2.6-0.4,4.6-2.5,4.9-5.2l16-115.8C464,134,464,133.1,463.8,132.2z"/>
                        </g>
                    </svg>
                </a> <!--end MetricaEcommerce-->   

                <a href="#MetricaCRM" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="CRM">
                    <svg class="nav-svg" version="1.1" id="Layer_3" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <g>
                                <path d="M276,68.1v219c0,3.7-2.5,6.8-6,7.7L81.1,343.4c-2.3,0.6-3.6,3.1-2.7,5.4C109.1,426,184.9,480.6,273.2,480
                                    C387.8,479.3,480,386.5,480,272c0-112.1-88.6-203.5-199.8-207.8C277.9,64.1,276,65.9,276,68.1z"/>
                            </g>
                            <path class="svg-primary" d="M32,239.3c0,0,0.2,48.8,15.2,81.1c0.8,1.8,2.8,2.7,4.6,2.2l193.8-49.7c3.5-0.9,6.4-4.6,6.4-8.2V36c0-2.2-1.8-4-4-4
                                C91,33.9,32,149,32,239.3z"/>
                        </g>
                    </svg>
                </a><!--end MetricaCRM-->

                <a href="#MetricaOthers" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="UI Kit">
                    <svg class="nav-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <path d="M70.7 164.5l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c8.9-4.3 8.9-11.3 0-15.6L272.1 67.2c-4.4-2.1-10.3-3.2-16.1-3.2s-11.7 1.1-16.1 3.2L70.7 148.9c-8.9 4.3-8.9 11.3 0 15.6z"/>
                        <path class="svg-primary" d="M441.3 248.2s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272 291.6 272 291.6c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.1 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9.1-4.2 9.1-11.2.2-15.5z"/>
                        <path d="M441.3 347.5s-30.9-14.9-35-16.9-5.2-1.9-9.5.1S272.1 391 272.1 391c-4.5 2.1-10.3 3.2-16.1 3.2s-11.7-1.1-16.1-3.2c0 0-117.3-56.6-122.8-59.3-6-2.9-7.7-2.9-13.1-.3l-33.4 16.1c-8.9 4.3-8.9 11.3 0 15.6l169.2 81.7c4.4 2.2 10.3 3.2 16.1 3.2s11.7-1.1 16.1-3.2l169.2-81.7c9-4.3 9-11.3.1-15.6z"/>
                    </svg>
                </a><!--end MetricaOthers-->

                <a href="#MetricaPages" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Pages">
                    <svg class="nav-svg" version="1.1" id="Layer_4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                        viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <path d="M462.5,352.3c-1.9-5.5-5.6-11.5-11.4-18.3c-10.2-12-30.8-29.3-54.8-47.2c-2.6-2-6.4-0.8-7.5,2.3l-4.7,13.4
                                c-0.7,2,0,4.3,1.7,5.5c15.9,11.6,35.9,27.9,41.8,35.9c2,2.8-0.5,6.6-3.9,5.8c-10-2.3-29-7.3-44.2-12.8c-8.6-3.1-17.7-6.7-27.2-10.6
                                c16-20.8,24.7-46.3,24.7-72.6c0-32.8-13.2-63.6-37.1-86.4c-22.9-21.9-53.8-34.1-85.7-33.7c-25.7,0.3-50.1,8.4-70.7,23.5
                                c-18.3,13.4-32.2,31.3-40.6,52c-8.3-6-16.1-11.9-23.2-17.6c-13.7-10.9-28.4-22-38.7-34.7c-2.2-2.8,0.9-6.7,4.4-5.9
                                c11.3,2.6,35.4,10.9,56.4,18.9c1.5,0.6,3.2,0.3,4.5-0.8l11.1-10.1c2.4-2.1,1.7-6-1.3-7.2C121,137.4,89.2,128,73.2,128
                                c-11.5,0-19.3,3.5-23.3,10.4c-7.6,13.3,7.1,35.2,45.1,66.8c34.1,28.5,82.6,61.8,136.5,92c87.5,49.1,171.1,81,208,81
                                c11.2,0,18.7-3.1,22.1-9.1C464.4,364.4,464.7,358.7,462.5,352.3z"/>
                            <path  class="svg-primary" d="M312,354c-29.1-12.8-59.3-26-92.6-44.8c-30.1-16.9-59.4-36.5-84.4-53.6c-1-0.7-2.2-1.1-3.4-1.1c-0.9,0-1.9,0.2-2.8,0.7
                                c-2,1-3.3,3-3.3,5.2c0,1.2-0.1,2.4-0.1,3.5c0,32.1,12.6,62.3,35.5,84.9c22.9,22.7,53.4,35.2,85.8,35.2c23.6,0,46.5-6.7,66.2-19.5
                                c1.9-1.2,2.9-3.3,2.7-5.5C315.5,356.8,314.1,354.9,312,354z"/>
                        </g>
                    </svg>                           
                </a><!--end MetricaPages-->

                <a href="#MetricaAuthentication" class="nav-link" data-toggle="tooltip-custom" data-placement="top" title="" data-original-title="Authentication">
                    <svg class="nav-svg" version="1.1" id="Layer_5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <g>
                            <path class="svg-primary" d="M376,192h-24v-46.7c0-52.7-42-96.5-94.7-97.3c-53.4-0.7-97.3,42.8-97.3,96v48h-24c-22,0-40,18-40,40v192c0,22,18,40,40,40
                                h240c22,0,40-18,40-40V232C416,210,398,192,376,192z M270,316.8v68.8c0,7.5-5.8,14-13.3,14.4c-8,0.4-14.7-6-14.7-14v-69.2
                                c-11.5-5.6-19.1-17.8-17.9-31.7c1.4-15.5,14.1-27.9,29.6-29c18.7-1.3,34.3,13.5,34.3,31.9C288,300.7,280.7,311.6,270,316.8z
                                    M324,192H188v-48c0-18.1,7.1-35.1,20-48s29.9-20,48-20s35.1,7.1,48,20s20,29.9,20,48V192z"/>
                        </g>
                    </svg>
                </a> <!--end MetricaAuthentication--> 

            </nav><!--end nav-->
        </div><!--end main-icon-menu-->

        <div class="main-menu-inner">
            <div class="menu-body slimscroll">
                <div id="MetricaAnalytic" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">Analytics</h6>       
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="{{ url('home') }}"><i class="dripicons-meter"></i>Dashboard</a></li>
                        <li class="nav-item"><a class="nav-link" href="../analytics/analytics-customers.html"><i class="dripicons-user-group"></i>Customers</a></li>
                        <li class="nav-item"><a class="nav-link" href="../analytics/analytics-reports.html"><i class="dripicons-document"></i>Reports</a></li> 
                    </ul>
                </div><!-- end Analytic -->
                <div id="MetricaCrypto" class="main-icon-menu-pane {{ Request::is('services/*') ? 'active mm-active' : '' }}">
                    <div class="title-box">
                        <h6 class="menu-title">Services</h6>
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="{{ url('services/view/all') }}"><i class="dripicons-stack"></i>View All</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('services/add') }}"><i class="mdi mdi-briefcase-plus"></i>Add Service</a></li>
                    </ul>
                </div><!-- end Crypto -->
                <div id="employees" class="main-icon-menu-pane {{ Request::is('employees/*') ? 'active mm-active' : '' }}">
                    <div class="title-box">
                        <h6 class="menu-title">Employees</h6>        
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="{{ url('employees/view/all') }}"><i class="dripicons-user-group"></i>View All</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{ url('employees/add') }}"><i class="mdi mdi-account-multiple-plus-outline"></i>Add Employee</a></li>
                    </ul>
                </div><!-- end  Project-->
                <div id="MetricaEcommerce" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">Ecommerce</h6>           
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-index.html"><i class="dripicons-device-desktop"></i>Dashboard</a></li>
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-products.html"><i class="dripicons-view-apps"></i>Products</a></li>
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-product-list.html"><i class="dripicons-list"></i>Product List</a></li>
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-product-detail.html"><i class="dripicons-article"></i>Product Detail</a></li>
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-cart.html"><i class="dripicons-cart"></i>Cart</a></li>
                        <li class="nav-item"><a class="nav-link" href="../ecommerce/ecommerce-checkout.html"><i class="dripicons-card"></i>Checkout</a></li>
                    </ul>
                </div><!-- end Ecommerce -->
                <div id="MetricaCRM" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">CRM</h6>          
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="../crm/crm-index.html"><i class="dripicons-monitor"></i>Dashboard</a></li>
                        <li class="nav-item"><a class="nav-link" href="../crm/crm-contacts.html"><i class="dripicons-user-id"></i>Contacts</a></li>
                        <li class="nav-item"><a class="nav-link" href="../crm/crm-opportunities.html"><i class="dripicons-lightbulb"></i>Opportunities</a></li>
                        <li class="nav-item"><a class="nav-link" href="../crm/crm-leads.html"><i class="dripicons-toggles"></i>Leads</a></li>
                        <li class="nav-item"><a class="nav-link" href="../crm/crm-customers.html"><i class="dripicons-user-group"></i>Customers</a></li>
                    </ul>
                </div><!-- end CRM -->
                <div id="MetricaOthers" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">Others</h6>      
                    </div>
                    <ul class="nav metismenu" id="main_menu_side_nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-mail"></i><span class="w-100">Email</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/email-inbox.html">Inbox</a></li>
                                <li><a href="../others/email-read.html">Read Email</a></li>            
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-view-thumb"></i><span class="w-100">UI Elements</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/ui-bootstrap.html">Bootstrap</a></li>
                                <li><a href="../others/ui-animation.html">Animation</a></li>
                                <li><a href="../others/ui-avatar.html">Avatar</a></li>
                                <li><a href="../others/ui-clipboard.html">Clip Board</a></li>
                                <li><a href="../others/ui-files.html">File Manager</a></li>
                                <li><a href="../others/ui-ribbons.html">Ribbons</a></li>
                                <li><a href="../others/ui-dragula.html"><span>Dragula</span></a></li>
                                <li><a href="../others/ui-check-radio.html"><span>Check & Radio</span></a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-anchor"></i><span class="w-100">Advanced UI</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/advanced-rangeslider.html">Range Slider</a></li>
                                <li><a href="../others/advanced-sweetalerts.html">Sweet Alerts</a></li>
                                <li><a href="../others/advanced-nestable.html">Nestable List</a></li>
                                <li><a href="../others/advanced-ratings.html">Ratings</a></li>
                                <li><a href="../others/advanced-highlight.html">Highlight</a></li>
                                <li><a href="../others/advanced-session.html">Session Timeout</a></li>
                                <li><a href="../others/advanced-idle-timer.html">Idle Timer</a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-document"></i><span class="w-100">Forms</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/forms-elements.html">Basic Elements</a></li>
                                <li><a href="../others/forms-advanced.html">Advance Elements</a></li>
                                <li><a href="../others/forms-validation.html">Validation</a></li>
                                <li><a href="../others/forms-wizard.html">Wizard</a></li>
                                <li><a href="../others/forms-editors.html">Editors</a></li>
                                <li><a href="../others/forms-repeater.html">Repeater</a></li>
                                <li><a href="../others/forms-x-editable.html">X Editable</a></li>
                                <li><a href="../others/forms-uploads.html">File Upload</a></li>
                                <li><a href="../others/forms-img-crop.html">Image Crop</a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-graph-line"></i><span class="w-100">Charts</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/charts-apex.html">Apex</a></li>
                                <li><a href="../others/charts-morris.html">Morris</a></li>
                                <li><a href="../others/charts-chartist.html">Chartist</a></li>
                                <li><a href="../others/charts-flot.html">Flot</a></li>
                                <li><a href="../others/charts-peity.html">Peity</a></li>
                                <li><a href="../others/charts-chartjs.html">Chartjs</a></li>
                                <li><a href="../others/charts-sparkline.html">Sparkline</a></li>
                                <li><a href="../others/charts-knob.html">Jquery Knob</a></li>
                                <li><a href="../others/charts-justgage.html">JustGage</a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-view-list-large"></i><span class="w-100">Tables</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/tables-basic.html">Basic</a></li>
                                <li><a href="../others/tables-datatable.html">Datatables</a></li>
                                <li><a href="../others/tables-responsive.html">Responsive</a></li>
                                <li><a href="../others/tables-footable.html">Footable</a></li>
                                <li><a href="../others/tables-jsgrid.html">Jsgrid</a></li>
                                <li><a href="../others/tables-editable.html">Editable</a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-headset"></i><span class="w-100">Icons</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/icons-materialdesign.html">Material Design</a></li>
                                <li><a href="../others/icons-dripicons.html">Dripicons</a></li>
                                <li><a href="../others/icons-fontawesome.html">Font awesome</a></li>
                                <li><a href="../others/icons-themify.html">Themify</a></li>
                                <li><a href="../others/icons-typicons.html">Typicons</a></li>
                                <li><a href="../others/icons-emoji.html">Emoji <i class="em em-ok_hand"></i></a></li>
                                <li><a href="../others/icons-svg.html">SVG</a></li>
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-map"></i><span class="w-100">Maps</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/maps-google.html">Google Maps</a></li>
                                <li><a href="../others/maps-vector.html">Vector Maps</a></li>        
                            </ul>            
                        </li><!--end nav-item-->
                        <li class="nav-item">
                            <a class="nav-link" href="#"><i class="dripicons-article"></i><span class="w-100">Email Templates</span><span class="menu-arrow"><i class="mdi mdi-chevron-right"></i></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="../others/email-templates-basic.html">Basic Action Email</a></li>
                                <li><a href="../others/email-templates-alert.html">Alert Email</a></li>
                                <li><a href="../others/email-templates-billing.html">Billing Email</a></li>               
                            </ul>            
                        </li><!--end nav-item-->
                    </ul><!--end nav-->
                </div><!-- end Others -->

                <div id="MetricaPages" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">Pages</h6>        
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-profile.html"><i class="dripicons-user"></i>Profile</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-chat.html"><i class="dripicons-conversation"></i>Chat</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-contact-list.html"><i class="dripicons-user-id"></i>Contact List</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-tour.html"><i class="dripicons-rocket"></i>Tour</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-timeline.html"><i class="dripicons-clock"></i>Timeline</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-invoice.html"><i class="dripicons-document"></i>Invoice</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-treeview.html"><i class="dripicons-network-3"></i>Treeview</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-starter.html"><i class="dripicons-clipboard"></i>Starter Page</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-pricing.html"><i class="dripicons-article"></i>Pricing</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-blogs.html"><i class="dripicons-blog"></i>Blogs</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-faq.html"><i class="dripicons-question"></i>FAQs</a></li>
                        <li class="nav-item"><a class="nav-link" href="../pages/pages-gallery.html"><i class="dripicons-photo-group"></i>Gallery</a></li>
                    </ul>
                </div><!-- end Pages -->
                <div id="MetricaAuthentication" class="main-icon-menu-pane">
                    <div class="title-box">
                        <h6 class="menu-title">Authentication</h6>     
                    </div>
                    <ul class="nav">
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-login.html"><i class="dripicons-enter"></i>Log in</a></li>
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-register.html"><i class="dripicons-pencil"></i>Register</a></li>
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-recover-pw.html"><i class="dripicons-clockwise"></i>Recover Password</a></li>
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-lock-screen.html"><i class="dripicons-lock"></i>Lock Screen</a></li>
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-404.html"><i class="dripicons-warning"></i>Error 404</a></li>
                        <li class="nav-item"><a class="nav-link" href="../authentication/auth-500.html"><i class="dripicons-wrong"></i>Error 500</a></li>
                    </ul>
                </div><!-- end Authentication-->
            </div><!--end menu-body-->
        </div><!-- end main-menu-inner-->
    </div>
    <!-- end left-sidenav-->
