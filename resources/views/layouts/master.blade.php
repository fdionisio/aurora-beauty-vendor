<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title> @yield('title')  | Lexa - Responsive Bootstrap 4 Admin Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Lexa Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('/images/favicon.ico')}}">
    
     <!-- headerCss -->
    @yield('headerCss')

    <!-- Bootstrap Css -->
    <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="{{ asset('css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="{{ asset('css/style.css')}}" id="app-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/metisMenu.min.css') }}" rel="stylesheet" type="text/css" />

</head>

<body data-sidebar="dark">

    <!-- Begin page -->
    <div id="layout-wrapper">

         @include('layouts/partials/header')

         <div class="page-wrapper">

         @include('layouts/partials/sidebar')

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->


                <div class="page-content">
                    <div class="container-fluid">

                    <!-- content -->
                    @yield('content')


                    @include('layouts/partials/footer')

                    </div>
                    <!-- end main content-->
                </div>
                <!-- END layout-wrapper -->
        </div>
               
            <!-- JAVASCRIPT -->
            <script src="{{ asset('js/jquery.min.js')}}"></script>
            <script src="{{ asset('js/bootstrap.bundle.min.js')}}"></script>
            <script src="{{ asset('js/metisMenu.min.js')}}"></script>
            <script src="{{ asset('js/waves.min.js')}}"></script>
            <script src="{{ asset('js/jquery.slimscroll.min.js')}}"></script>

            <!-- footerScript -->
             @yield('footerScript')
            <!-- App js -->
            <script src="{{ asset('/js/app.js')}}"></script>
</body>
</html>