<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;
class ServiceImage extends Model
{
    protected $fillable = [
        'service_id',
        'image_file',
        'image_url', 
        'is_primary',
        'is_active'
    ];

    public function services() 
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }

    public function store($params)
    {
        $serviceImage = ServiceImage::create($params);
        return $serviceImage;
    }

    public function fetchByService($id)
    {
        $images = ServiceImage::with('services')
                                ->where('service_id', $id)
                                ->get();
        return $images->toArray();
    }
}
