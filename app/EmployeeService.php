<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Service;
class EmployeeService extends Model
{
    protected $fillable =[
        'employee_id',
        'service_id',
    ];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function services()
    {
        return $this->belongsTo(Service::class, 'service_id', 'id');
    }
}
