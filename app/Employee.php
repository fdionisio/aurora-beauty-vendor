<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable =[
        'vendor_id',
        'first_name',
        'last_name',
        'email',
        'contact_number',
        'profile_photo',
        'is_active'
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Vendor')->withDefault();  
    }

    public function schedule()
    {
        return $this->hasOne('App\EmployeeSchedule');
    }

    public function service()
    {
        return $this->hasMany('App\EmployeeService');
    }

    public function leaves()
    {
        return $this->hasMany('App\EmployeeLeave');
    }

    public function fetchAll($id)
    {
        $employees = Employee::with('schedule')
                            ->where('vendor_id', $id)
                            ->get();
        return $employees;
    }

    public function fetch($id)
    {
        $employee = Employee::with('schedule')
                            ->with('service.services')
                            ->with('leaves')
                            ->where('id', $id)
                            ->get();
        return $employee;
    }
}
