<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreEmployee extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'required|max:256|alpha',
            'last_name' => 'required|max:256|alpha',
            'email' => 'required|email|max:256',
            'contact_number' => 'required|max:256',
            'start_work_hour' => 'required|date_format:H:i',
            'end_work_hour' => 'required|date_format:H:i',
            'profile_photo' => 'image',
            'is_active' => 'required|boolean',
            'employee_leave' => 'max:10',
            'employee_service' => 'required'
        ];
    }
}
