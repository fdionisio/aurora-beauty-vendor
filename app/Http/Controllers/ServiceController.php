<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\ServiceImage;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\ServiceCategory;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Service $service)
    {
        $services = $service->fetchAll(Auth::user()['id'])->toArray();
        return view('services.viewAll', compact('services'));
    }

    public function create(ServiceCategory $category)
    {
        $categories = $category::all();
        return view('services.add', compact('categories'));
    }

    public function store(Request $request, Service $service, ServiceImage $image)
    {
        try{
            $serviceResponse = $service::create([
                        'vendor_id' => Auth::user()['id'],
                        'service_category_id' => $request->post('service_category_id'),
                        'service_name' => $request->post('service_name'),
                        'service_description' => $request->post('service_description'),
                        'price' => $request->post('price'),
                        'is_active' =>  1,
            ]);

            if ($request->hasFile('primary_image')) {
                $path = $request->file('primary_image')->store('public/services');
                $imageFile = basename($path);
                $file = env('APP_URL') . 'storage/services/' . basename($path);
                $imageURL = $file;
                $params = [
                        'service_id' => $serviceResponse->id,
                        'image_file' => $imageFile,
                        'image_url' => $imageURL,
                        'is_primary' => true,
                ];
                $primaryImage = $image->store($params);
            }
          
            if ($request->hasFile('secondary_images')) {
                foreach($request->file('secondary_images') as $secondaryImage){
                    $path = $secondaryImage->store('public/services');
                    $imageURL = env('APP_URL') . 'storage/services/' . basename($path);
                    $params = [
                            'service_id' => $serviceResponse->id,
                            'image_file' => basename($path),
                            'image_url' => $imageURL,
                            'is_primary' => false,
                    ];
                    $secondaryImage = $image->store($params);
                }
            }

            return back()->with('success', 'Service Successfully Added.');
        } catch(ModelNotFoundException $e) {
            return back()->with('error', 'Something went wrong. Please try again.');
        }
    }

    public function show(Service $service, ServiceImage $images, $id)
    {
        $serviceId = Crypt::decryptString($id);
        $services = $service->fetch($serviceId);
        $service = $services[0];
        $images = $images->fetchByService($serviceId);
        return view('services.view', compact('service', 'images'));
    }

    public function edit(Service $service, ServiceCategory $category, $id)
    {
        $serviceId = Crypt::decryptString($id);
        $services = $service->fetch($serviceId)->toArray();
        $service = $services[0];
        $categories = $category::all();
        return view('services.update', compact('service', 'categories'));
    }

    public function update(Request $request, Service $service, $id)
    {
        $serviceId = Crypt::decryptString($id);
        try{
            $response = $service::where('id', $serviceId)
                                        ->update([
                                            'service_category_id' => $request->post('service_category_id'),
                                            'service_name' => $request->post('service_name'),
                                            'service_description' => $request->post('service_description'),
                                            'price' => $request->post('price'),
                                            'is_active' => $request->post('is_active'),
                                        ]);
            return back()->with('success', 'Service Record Successfully Updated.');
        } catch(ClientException $e) {
            return back()->with('error', 'Something went wrong. Please try again later.');
        }     
    }

    public function destroy($id)
    {
        //
    }
}
