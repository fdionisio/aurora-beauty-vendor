<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Employee;
use App\EmployeeLeave;
use App\EmployeeService;
use App\EmployeeSchedule;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreEmployee;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{

    public function index(Employee $employee)
    {
        $employees = $employee->fetchAll(Auth::user()['id'])->toArray();
        return view('employees.viewAll', compact('employees'));
    }

    public function create(Service $service)
    {
        $services = $service->fetchAll(Auth::user()['id'])->toArray();
        return view('employees.add', compact('services'));
    }

    public function store(StoreEmployee $request, Employee $employee, EmployeeLeave $leave, EmployeeSchedule $schedule, EmployeeService $service)
    {
        try{
            if ($request->hasFile('profile_photo')) {
                $path = $request->file('profile_photo')->store('public/employees');
                $imageFile = basename($path);
                $profilePhoto = env('APP_URL') . 'storage/employees/' . basename($path);
            }

            $employee = $employee::create([
                'vendor_id' => Auth::user()['id'],
                'first_name' => $request->post('first_name'),
                'last_name' => $request->post('last_name'),
                'email' => $request->post('email'),
                'contact_number' => $request->post('contact_number'),
                'profile_photo' => $profilePhoto,
                'is_active' => $request->post('is_active'),
            ]);

            $employeeSchedule = $schedule::create([
                'employee_id' => $employee->id,
                'start_work_hour' => $request->post('start_work_hour'),
                'end_work_hour' => $request->post('end_work_hour'),
                'on_leave' => FALSE,
                'on_appointment' => FALSE,
            ]);

            $employeeLeaves = $request->post('employee_leave');
            if(!empty($employeeLeaves))
            {
                foreach($employeeLeaves as $employeeLeave){
                    $params = [
                        'employee_id' => $employee->id,
                        'day' => $employeeLeave
                    ];
                    $insert = $leave::create($params);
                }
            }

            $employeeServices = $request->post('employee_service');
            if($employeeServices){
                foreach($employeeServices as $employeeService)
                {
                    $params = [
                        'employee_id' => $employee->id,
                        'service_id' => $employeeService
                    ];
                    $insert = $service::create($params);
                }
            }   
            return back()->with('success', 'Employee Successfully Added.');
        } catch(Throwable $e) {
            return back()->with('error', $e);
        }
    }

    public function show($id, Employee $employee)
    {
        $employeeId = Crypt::decryptString($id);
        $employee = $employee->fetch($employeeId)->toArray();
        return view('employees.view', compact('employee'));
    }

    public function edit($id, Employee $employee, Service $service)
    {
        $employeeId = Crypt::decryptString($id);
        $employee = $employee->fetch($employeeId)->toArray();
        $services = $service->fetchAll(Auth::user()['id'])->toArray();
        return view('employees.update', compact('employee', 'services'));
    }

    public function update(Request $request, $id, Employee $employee, EmployeeLeave $leave, EmployeeSchedule $schedule, EmployeeService $service)
    {
        try {
            $employeeId = Crypt::decryptString($id);
            $oldImage = $request->post('old_image');
            if ($request->hasFile('profile_photo')) {
                $path = $request->file('profile_photo')->store('public/employees');
                Storage::disk('public')->delete('employees/' . basename($oldImage));
                $imageFile = basename($path);
                $profilePhoto = env('APP_URL') . 'storage/employees/' . basename($path);
                $employee = $employee::where('id', $employeeId)
                                ->update([
                                    'profile_photo' => $profilePhoto,
                                ]);
            }
            // Update Employee Record
            $employee = $employee::where('id', $employeeId)
                                ->update([
                                    'first_name' => $request->post('first_name'),
                                    'last_name' => $request->post('last_name'),
                                    'email' => $request->post('email'),
                                    'contact_number' => $request->post('contact_number'),
                                    'is_active' => $request->post('is_active'),
                                ]);
            // Update Employee Schedule Record                    
            $employeeSchedule = $schedule::where('employee_id', $employeeId)
                                        ->update([
                                            'start_work_hour' => $request->post('start_work_hour'),
                                            'end_work_hour' => $request->post('end_work_hour'),
                                        ]);
            
            // Update Employee Leaves
            $employeeLeaves = $request->post('employee_leave');
            if(!empty($employeeLeaves))
            {
                $leave::where('employee_id', $employeeId)->delete();
                foreach($employeeLeaves as $employeeLeave){
                    $params = [
                        'employee_id' => $employeeId,
                        'day' => $employeeLeave
                    ];
                    $insert = $leave::create($params);
                }
            }


            $employeeServices = $request->post('employee_service');
            if($employeeServices){
                $service::where('employee_id', $employeeId)->delete();
                foreach($employeeServices as $employeeService)
                {
                    $params = [
                        'employee_id' => $employeeId,
                        'service_id' => $employeeService
                    ];
                    $insert = $service::create($params);
                }
            }   

            return back()->with('success', 'Employee Record Successfully Updated.');
        }catch(ClientException $e) {
            return back()->with('error', $e);
        }
    }

    public function destroy($id)
    {
        //
    }
}
