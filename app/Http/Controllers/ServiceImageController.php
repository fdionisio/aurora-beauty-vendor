<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceImage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class ServiceImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    public function create($id)
    {
        $serviceId = Crypt::decryptString($id);
        return view('services.images.add', compact('serviceId'));
    }

    public function store(Request $request, ServiceImage $images, $id)
    {
        $serviceId = Crypt::decryptString($id);
        try{
            if ($request->hasFile('primary_image')) {
                $path = $request->file('primary_image')->store('public/services');
                $imageFile = basename($path);
                $file = env('APP_URL') . 'storage/services/' . basename($path);
                $imageURL = $file;
                $params = [
                        'service_id' => $serviceId,
                        'image_file' => $imageFile,
                        'image_url' => $imageURL,
                        'is_primary' => true,
                ];
                $primaryImage = $images->store($params);
            }
          
            if ($request->hasFile('secondary_images')) {
                foreach($request->file('secondary_images') as $secondaryImage){
                    $path = $secondaryImage->store('public/services');
                    $imageURL = env('APP_URL') . 'storage/services/' . basename($path);
                    $params = [
                            'service_id' =>  $serviceId,
                            'image_file' => basename($path),
                            'image_url' => $imageURL,
                            'is_primary' => false,
                    ];
                    $secondaryImage = $images->store($params);
                }
            }

            return back()->with('success', 'Service Images Successfully Added.');
        } catch(ClientException $e) {
            return back()->with('error', 'Something went wrong. Please try again.');
        }
    }

    public function show(ServiceImage $images, $id)
    {
        $imageId = Crypt::decryptString($id);
        $image = $images::findOrFail($imageId);
        return view('services.images.update', compact('image'));
    }

    public function edit(ServiceImage $images, $id)
    {
        $serviceId = Crypt::decryptString($id);
        $images = $images->fetchByService($serviceId);
        return view('services.images.view', compact('images', 'serviceId'));
    }

    public function update(Request $request, ServiceImage $images, $id)
    {
        $oldImage = $request->post('old_image');
        $serviceId = $request->post('service_id');
        $imageId = Crypt::decryptString($id);
        try{
            if ($request->hasFile('service_image')) {
                $path = $request->file('service_image')->store('public/services');
                Storage::disk('public')->delete('services/' . $oldImage);
                $imageFile = basename($path);
                $file = env('APP_URL') . 'storage/services/' . basename($path);
                $imageURL = $file;
                $params = [
                    'service_id' => $serviceId,
                    'image_file' => $imageFile,
                    'image_url' => $imageURL,
                ];
                $response = $images::where('id', $imageId)
                                    ->update($params);
            }
            if($request->post('is_primary') == TRUE)
            {
                $response = $images::where('service_id', $serviceId)
                                   ->update(['is_primary' => FALSE]);
                $response = $images::where('id', $imageId)
                                    ->update(['is_primary' => TRUE]);
            }
            return back()->with('success', 'Service Image Record Successfully Updated.');
        } catch(ClientException $e) {
            return back()->with('error', 'Something went wrong. Please try again later.');
        }   
    }

    public function insert(Request $request, ServiceImage $image, $id)
    {
        $serviceId = Crypt::decryptString($id);
        return view('services.images.insert', compact('serviceId'));
    }

    public function insertServiceImage(Request $request, ServiceImage $image, $id)
    {
        $serviceId = Crypt::decryptString($id);
        try{
            if ($request->hasFile('service_image')) {
                $path = $request->file('service_image')->store('public/services');
                $imageFile = basename($path);
                $file = env('APP_URL') . 'storage/services/' . basename($path);
                $imageURL = $file;
                
                if($request->post('is_primary') == TRUE)
                {
                    $response = $image::where('service_id', $serviceId)
                                    ->update(['is_primary' => FALSE]);
                    $isPrimary = TRUE;
                }
                else{
                    $isPrimary = FALSE;
                }

                $params = [
                        'service_id' => $serviceId,
                        'image_file' => $imageFile,
                        'image_url' => $imageURL,
                        'is_primary' => $isPrimary,
                ];
                $primaryImage = $image->store($params);
            }
            return back()->with('success', 'Service Image Successfully Added.');
        } catch(ClientException $e) {
            return back()->with('error', $e->getMessage()); 
        }
    }
}
