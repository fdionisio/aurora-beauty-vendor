<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeSchedule extends Model
{
    protected $table = 'employee_schedule';
    protected $fillable =[
        'employee_id',
        'start_work_hour',
        'end_work_hour',
        'on_leave',
        'on_appointment',
    ];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
