<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ServiceImage;
use App\EmployeeServices;
class Service extends Model
{
    protected $fillable = [
        'vendor_id',
        'service_name',
        'service_description',
        'price',
        'is_active',
    ];

    public function fetchAll($id)
    {
        $services = Service::with('serviceImages')
                            ->where('vendor_id', $id)
                            ->with('serviceCategories')
                            ->get();
        return $services;
    }

    public function fetch($id)
    {
        $services = Service::with('serviceImages')
                            ->where('id', $id)
                            ->get();
        return $services;
    }

    public function serviceImages() 
    {
        return $this->hasMany(ServiceImage::class, 'service_id', 'id');
    }

    public function serviceCategories()
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id', 'id');
    }

}
