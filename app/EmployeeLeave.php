<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeave extends Model
{
    protected $fillable =[
        'employee_id',
        'day'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
