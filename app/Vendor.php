<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Service;
class Vendor extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'store_name',
        'contact_person',
        'email', 
        'phone_number',
        'mobile_number',
        'address1',
        'address2',
        'barangay',
        'city',
        'province_state',
        'postal_code',
        'country',
        'password',
        'is_active',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function services()
    {
        return $this->hasMany(Service::class, 'vendor_id', 'id');
    }
}
