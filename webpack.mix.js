const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/style.scss', 'public/css')
    .sass('resources/sass/icons.scss', 'public/css')
    .js('resources/js/bootstrap.bundle.min.js', 'public/js')
    .js('resources/js/jquery.core.js', 'public/js')
    .js('resources/js/jquery.min.js', 'public/js')
    .js('resources/js/jquery.slimscroll.min.js', 'public/js');
    
