<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsToNullableInVendorsTable extends Migration
{

    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('address1', 512)->nullable()->change();
            $table->string('barangay', 512)->nullable()->change();
            $table->string('city', 256)->nullable()->change();
            $table->string('province_state', 256)->nullable()->change();
            $table->string('postal_code', 50)->nullable()->change();
            $table->string('country', 256)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            //
        });
    }
}
