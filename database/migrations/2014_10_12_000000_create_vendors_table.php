<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{

    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('store_name', 256)->unique();
            $table->string('store_logo')->nullable();
            $table->string('store_banner')->nullable();
            $table->string('contact_person', 256);
            $table->string('email')->unique();
            $table->string('phone_number')->unique();
            $table->string('mobile_number')->unique();
            $table->string('address1', 512);
            $table->string('address2', 512)->nullable();
            $table->string('barangay', 512);
            $table->string('city', 256);
            $table->string('province_state', 256);
            $table->string('postal_code', 50);
            $table->string('country', 256);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('is_active');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
