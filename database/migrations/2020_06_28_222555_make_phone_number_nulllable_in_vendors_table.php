<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakePhoneNumberNulllableInVendorsTable extends Migration
{

    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('phone_number')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            //
        });
    }
}
