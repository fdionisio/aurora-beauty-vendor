<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCharacterLimitOfcontactNumbersInVendorsTable extends Migration
{

    public function up()
    {
        Schema::table('vendors', function (Blueprint $table) {
            $table->string('phone_number', 50)->change();
            $table->string('mobile_number', 50)->change();
        });
    }

    public function down()
    {
        Schema::table('vendors', function (Blueprint $table) {
            //
        });
    }
}
