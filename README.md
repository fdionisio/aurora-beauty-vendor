## Aurora Beauty Web Application

Aurora Beauty is a booking system for beauty services 

## Local setup 

Requirements: 
Composer 
PHP 
MySQL 

## Step 1
Fetch codebase of aurora-beauty in git lab 

```
git clone git@gitlab.com:fdionisio/aurora-beauty-vendor.git
cd aurora-beauty
```
## Step 2

Install dependencies using Composer 
```
composer install
```

## Step 3 
Setup .env file from .env.example
```
cp .env.example .env
cp src/.env.example src/.env
```

## Step 4  
Migrate database 
```
php artisan migrate
```


## Step 5
Run application
```
php artisan serve
```




