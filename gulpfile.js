const gulp = require('gulp');
var browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const watch = require('gulp-watch');
const del = require('del');

gulp.task('sass', () => {
    return gulp.src('resources/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.stream());
});

gulp.task('clean', () => {
    return del([
        'public/style.css',
    ]);
});

gulp.task('default', gulp.series(['clean', 'sass']));

gulp.task('watch', function(){

    browserSync.init({
        proxy: "localhost:8001"
    });

    gulp.watch('resources/sass/*.scss', gulp.series('sass')); 
    gulp.watch('resources/views/*.blade.php').on('change', browserSync.reload);
    gulp.watch('resources/views/auth/*.blade.php').on('change', browserSync.reload);
    gulp.watch('public/js/*.js').on('change', browserSync.reload);  
    gulp.watch('public/pages/*.js').on('change', browserSync.reload); 
    gulp.watch('resources/views/employees/*.blade.php').on('change', browserSync.reload);
    gulp.watch('resources/views/services/*.blade.php').on('change', browserSync.reload);
    gulp.watch('resources/views/services/images/*.blade.php').on('change', browserSync.reload);

    /* Controllers */
    gulp.watch('app/Http/Controllers/*.php').on('change', browserSync.reload); 

})