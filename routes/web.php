<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*
|--------------------------------------------------------------------------
| Services Routes
|--------------------------------------------------------------------------
*/
Route::prefix('services/')->group(function () {
    Route::get('view/all', 'ServiceController@index');
    Route::get('add', 'ServiceController@create');
    Route::post('add', 'ServiceController@store');
    Route::get('update/{id}', 'ServiceController@edit');
    Route::get('{id}', 'ServiceController@show');
    Route::post('update/{id}', 'ServiceController@update');
});

/*
|--------------------------------------------------------------------------
| Service Images Routes
|--------------------------------------------------------------------------
*/
Route::prefix('services/images/')->group(function () {
    Route::get('manage/{id}', 'ServiceImageController@edit');
    Route::get('update/{id}', 'ServiceImageController@show');
    Route::post('update/{id}', 'ServiceImageController@update');
    Route::get('add/{id}', 'ServiceImageController@create');
    Route::post('add/{id}', 'ServiceImageController@store');
    Route::get('insert/{id}', 'ServiceImageController@insert');
    Route::post('insert/{id}', 'ServiceImageController@insertServiceImage');
});

/*
|--------------------------------------------------------------------------
| Employees Routes
|--------------------------------------------------------------------------
*/
Route::prefix('employees/')->group(function () {
    Route::get('view/all', 'EmployeeController@index');
    Route::get('add', 'EmployeeController@create');
    Route::post('add', 'EmployeeController@store');
    Route::get('view/{id}', 'EmployeeController@show');
    Route::get('update/{id}', 'EmployeeController@edit');
    Route::post('update/{id}', 'EmployeeController@update');
});
